from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory,Account,Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipt_list(request):
    lists = Receipt.objects.filter(purchaser=request.user.id)
    #items = ExpenseCategory.objects.all()
    #account = Account.objects.all()
    #lists = get_object_or_404(Receipt, id=id)
    context = {
        'lists': lists,
        #'items': items,
        #'account': account,
    }
    return render(request, 'receipts/receipt_list.html', context)
# Create your views here.

@login_required
def create_post(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect('home')
    else:
        form = ReceiptForm()

    context={ 'form': form,}
    return render(request, 'receipts/create.html', context)

@login_required
def category_list(request):
    #lists = Receipt.objects.filter(purchaser=request.user.id)
    #lists = Receipt.objects.all()
    items = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        #'lists': lists,
        'items': items,
    }
    return render(request,'receipts/category_list.html', context)

@login_required
def account_list(request):
    #lists = Receipt.objects.filter(purchaser=request.user.id)
    #lists = Receipt.objects.all()
    accounts = Account.objects.filter(owner=request.user)
    context = {
        #'lists': lists,
        'accounts': accounts,
    }
    return render(request,'receipts/account_list.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect('category')
    else:
        form = CategoryForm()

    context={ 'form': form,}
    return render(request, 'receipts/create_category.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect('accounts')
    else:
        form = AccountForm()

    context={ 'form': form,}
    return render(request, 'receipts/create_account.html', context)
