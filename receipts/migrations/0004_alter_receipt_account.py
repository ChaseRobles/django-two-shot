# Generated by Django 5.0.1 on 2024-02-01 01:16

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0003_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipt",
                to="receipts.account",
            ),
        ),
    ]
